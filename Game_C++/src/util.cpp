#include "includes.h"
#include "util.h"

// trim from start
std::string &ltrim(std::string &s) {
	s.erase(s.begin(),
			std::find_if(s.begin(), s.end(),
					std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
std::string &rtrim(std::string &s) {
	s.erase(
			std::find_if(s.rbegin(), s.rend(),
					std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
			s.end());
	return s;
}

// trim from both ends
std::string &trim(std::string &s) {
	return ltrim(rtrim(s));
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::string word;
	std::vector<std::string> words;
	int start = 0, end;
	while ((end = s.find(delim, start)) != std::string::npos) {
		word = s.substr(start, end - start);
		if (word != "")
			words.push_back(word);
		start = end + 1;
	}
	word = s.substr(start, s.size() - start);
	if (word != "")
		words.push_back(word);
	return words;
}
