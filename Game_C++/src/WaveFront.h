#ifndef OBJ_H_
#define OBJ_H_

#include "includes.h"

class Mtl {
private:
	GLfloat ns; // Specularity constant
	std::vector<GLfloat> ka; // Ambient color
	std::vector<GLfloat> kd; // Diffuse color
	std::vector<GLfloat> ks; // Specular color
	GLfloat ni; // Index of refraction
	GLfloat d; // Transparency
	GLint illum; // Illumination model
public:
	std::string name;
	Mtl(std::string name);
	Mtl();
	virtual ~Mtl();
	void bind();
	void setSpecular(GLfloat ns) {
		this->ns = ns;
	}
	void setAmbientColor(std::vector<GLfloat> ka) {
		this->ka = ka;
	}
	void setDiffuseColor(std::vector<GLfloat> kd) {
		this->kd = kd;
	}
	void setSpecularColor(std::vector<GLfloat> ks) {
		this->ks = ks;
	}
	void setIOR(GLfloat ni) {
		this->ni = ni;
	}
	void setTransparency(GLfloat d) {
		this->d = d;
	}
	void setIllumModel(GLint illum) {
		this->illum = illum;
	}
};

typedef struct {
	double x;
	double y;
	double z;
} Vertex;

typedef struct {
	std::vector<GLuint> vIndices;
	std::shared_ptr<Mtl> mtl;
} Face;

class Obj {
private:
	std::vector<Face> faces;
public:
	std::string name;
	Obj(std::string name);
	virtual ~Obj();
	void draw();
	void addFace(Face face) {
		this->faces.push_back(face);
	}
};

class WaveFront {
public:
	std::map<std::string, std::shared_ptr<Obj>> objects;
	std::map<std::string, std::shared_ptr<Mtl>> materials;
	std::vector<Vertex> vertices;
	std::vector<Vertex> normals;
public:
	WaveFront();
	virtual ~WaveFront();
	void bind() {
		glVertexPointer(3, GL_DOUBLE, 0, vertices.data());
		glNormalPointer(GL_DOUBLE, 0, normals.data());
	}
	void draw();
	void finish();
	static WaveFront load(std::string fname);
	static void loadMtl(std::string fname, WaveFront const& loaded);
	std::shared_ptr<Obj> getObj(std::string objname) {
		return objects[objname];
	}
	void printVerts() {
		for (int i = 0; i < vertices.size(); i++) {
			if (i > 0 && (i % 3) == 0)
				std::cout << '\n';
			Vertex vtx = vertices[i];
			std::cout << '(' << vtx.x << ',' << vtx.y << ',' << vtx.z << ')' << ' ';
		}
	}
};

#endif /* OBJ_H_ */
