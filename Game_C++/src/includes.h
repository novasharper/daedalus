/*
 * includes.h
 *
 *  Created on: Jul 5, 2013
 *      Author: pat
 */

#ifndef __GXX_EXPERIMENTAL_CXX0X__
#define __GXX_EXPERIMENTAL_CXX0X__
#endif

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include <windows.h>
#include <GL/glew.h>

#define GLFW_INCLUDE_GLU
#define GLFW_DLL
#include <GLFW/glfw3.h>
#include <GL/glut.h>

#include <map>
#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <sstream>
#include <utility>
#include <locale>
#include <fstream>
#include <bits/move.h>
#include <algorithm>
#include <functional>
#include <cctype>
#include <memory>
#include <math.h>
#include <stdarg.h>
#include <glm/glm.hpp>

#define PI 3.14159

#endif /* INCLUDES_H_ */
