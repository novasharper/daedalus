/*
 * util.h
 *
 *  Created on: Jul 4, 2013
 *      Author: pat
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "includes.h"

std::string &ltrim(std::string &s);
std::string &rtrim(std::string &s);
std::string &trim(std::string &s);
std::vector<std::string> split(const std::string &s, char delim);

#endif /* UTIL_H_ */
