#include <windows.h>
#include "Game.h"
#include "WaveFront.h"
#include <iostream>

int main(int argc, char** argv) {
	if (!glfwInit()) {
		std::cout << "Failed to init GLFW";
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES,4);

	GLFWwindow* window = glfwCreateWindow(Game::WIDTH, Game::HEIGHT, "Hello World", NULL, NULL);
	glfwMakeContextCurrent(window);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwSwapInterval(1);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cout << "Failed to init GLEW " << err;
		return -1;
	}

//	glutInit(&argc, argv);

	Game* game = new Game(window);
	game->start();

	return 0;
}
