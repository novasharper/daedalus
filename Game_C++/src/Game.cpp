/*
 * Game.cpp
 *
 *  Created on: Jul 3, 2013
 *      Author: pat
 */

#include "includes.h"
#include "Game.h"

std::vector<GLbyte> Game::keys(256);

Game::Game(GLFWwindow* window) {
	this->window = window;
	glfwSetKeyCallback(window, Game::handleKeyboard);
	glfwSetFramebufferSizeCallback(window, Game::handleResize);
	model = WaveFront::load("Debug/pistolBlendSwap.obj");
}

Game::~Game() {
	// TODO Auto-generated destructor stub
}

void Game::display() {
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();

	double r = 20;
	gluLookAt(r * sin(angle), 0.5, r * cos(angle), 0, 0.5, 0, 0, 1, 0);
	//glTranslated(0, 0.5, 0);
	glRotated(angle2 * 180 / PI, r * sin(angle + PI / 2), 0, r * cos(angle + PI / 2));
	//glTranslated(0, -0.5, 0);
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

//	model.getObj("shoulder_r")->draw();

//	glutSolidTeapot(1);
	model.draw();

	glPopMatrix();

	glfwSwapBuffers(window);
}

void Game::start() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, ((GLdouble) WIDTH) / HEIGHT, 0.1, 500);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_BLEND);

//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	GLfloat light_position[] = { 3.0, 3.0, 3.0, 0.0 };
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_COLOR_MATERIAL);

//	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
//	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 2.0);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1.0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.5);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);



//	model.getObj("torso")->printVerts();
	double delta = 0, delta2 = 0;
	while (!glfwWindowShouldClose(window)) {
		if(angle > PI*2) angle = 0;
		else if(angle < 0) angle = PI*2;
		if(Game::keys[GLFW_KEY_LEFT] == GLFW_PRESS) delta = 0.02;
		else if(Game::keys[GLFW_KEY_RIGHT] == GLFW_PRESS) delta = -0.02;
		else if(Game::keys[GLFW_KEY_LEFT] == GLFW_RELEASE && Game::keys[GLFW_KEY_RIGHT] == GLFW_RELEASE) delta = 0;
		angle += delta;
		if(angle2 > PI*2) angle2 = 0;
		else if(angle2 < 0) angle2 = PI*2;
		if(Game::keys[GLFW_KEY_UP] == GLFW_PRESS) delta2 = 0.02;
		else if(Game::keys[GLFW_KEY_DOWN] == GLFW_PRESS) delta2 = -0.02;
		else if(Game::keys[GLFW_KEY_UP] == GLFW_RELEASE && Game::keys[GLFW_KEY_DOWN] == GLFW_RELEASE) delta2 = 0;
		angle2 += delta2;
		display();
		glfwPollEvents();
	}
	glfwTerminate();
}

void Game::handleMouseMove(GLFWwindow* window, double xpos, double ypos) {

}

void Game::handleKeyboard(GLFWwindow* window, int key, int scancode, int action,
		int mods) {
	if (key == GLFW_KEY_ESCAPE)
		glfwSetWindowShouldClose(window, GL_TRUE);
	Game::keys[key] = action;
}

void Game::handleResize(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, ((GLdouble)width)/height, 0.1, 500);
	glMatrixMode(GL_MODELVIEW);
}
