#include "includes.h"
#include "WaveFront.h"
#include "util.h"

Mtl::Mtl(std::string name) {
	this->name = name;
}

Mtl::~Mtl() {
}

void Mtl::bind() {
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, ns);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, kd.data());
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ka.data());
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, ks.data());
}

Obj::Obj(std::string objName) {
	name = objName;
}

Obj::~Obj() {
	// TODO Auto-generated destructor stub
}

void Obj::draw() {
	for (unsigned int i = 0; i < faces.size(); i++) {
		Face f = faces[i];
		if(f.mtl.get() != NULL) f.mtl->bind();
		glDrawElements(GL_TRIANGLE_FAN, f.vIndices.size(), GL_UNSIGNED_INT,
				&f.vIndices.data()[0]);
	}
}

WaveFront::WaveFront() {
}

WaveFront::~WaveFront() {
}

void WaveFront::draw() {
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
	bind();
	typedef std::map<std::string, std::shared_ptr<Obj>> ObjMap;
	ObjMap::const_iterator end = objects.end();
	for (ObjMap::const_iterator it = objects.begin(); it != end; ++it) {
		it->second->draw();
	}
    glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_CULL_FACE);
}

void WaveFront::finish() {
}

WaveFront WaveFront::load(std::string fname) {
	WaveFront loaded;
	std::ifstream in;
	in.open(fname.c_str());
	std::string line;
	if (in.is_open()) {
		Obj* currentObj = NULL;
		std::shared_ptr<Mtl> currentMtl = NULL;
		std::vector<Face> faces;
		std::vector<Vertex> vertexBuffer;
		int vindex = 0;
		do {
			getline(in, line);
			line = trim(line);
			if ((line.length() == 0)
					|| (line.find_first_not_of(" \t\r\n") == std::string::npos)
					|| (line[0] == '#')) {
				continue;
			}
			std::vector<std::string> parts = split(line, ' ');
			if (parts[0] == "mtllib") {
				loadMtl(parts[1], loaded);
			} else if (parts[0] == "usemtl") {
				currentMtl = loaded.materials[parts[1]];
			} else if (parts[0] == "o") {
				if (currentObj != NULL) {
					loaded.objects.insert(
							std::pair<std::string, std::shared_ptr<Obj>>(
									currentObj->name,
									std::shared_ptr<Obj>(currentObj)));
				}
				std::string name = parts[1];
				currentObj = new Obj(name);
			} else if (parts[0] == "v") {
				if (parts.size() != 4)
					throw 1;
				Vertex vtx;
				for (int i = 1; i <= 3; i++) {
					((double*)(&vtx))[i-1] = atof(parts[i].c_str());
				}
				vertexBuffer.push_back(vtx);
			} else if (parts[0] == "f") {
				if (currentObj == NULL)
					continue;
				std::vector<GLuint> vert;
				Vertex vNormal;
				std::vector<Vertex> vNormals(parts.size() - 1);

				for (unsigned int i = 1; i < parts.size(); i++) {
					int _vindex = atoi(parts[i].c_str()) - 1;
					int _vindex2 = atoi(parts[i % (parts.size() - 1) + 1].c_str()) - 1;

					Vertex current = vertexBuffer[_vindex];
					Vertex next = vertexBuffer[_vindex2];

					loaded.vertices.push_back(current);
					vert.push_back(vindex);

					vNormal.x += (current.x - next.y) * (current.z + next.z);
					vNormal.y += (current.y - next.z) * (current.x + next.x);
					vNormal.z += (current.z - next.x) * (current.y + next.y);
					vindex++;
				}
				glm::vec3 normal(vNormal.x, vNormal.y, vNormal.z);
				normal = glm::normalize(normal);

				for(int i = 0; i < parts.size() - 1; i++) loaded.normals.push_back(Vertex { normal.x, normal.y, normal.z });
				currentObj->addFace(Face { vert, currentMtl });
			}
		} while (in);
		if (currentObj != NULL) {
			loaded.objects.insert(
					std::pair<std::string, std::shared_ptr<Obj>>(
							currentObj->name,
							std::shared_ptr<Obj>(currentObj)));
		}
	}
	loaded.finish();
	in.close();
	return loaded;
}

void WaveFront::loadMtl(std::string fname, WaveFront const& loaded) {
	std::ifstream in;
	in.open(fname.c_str());
	std::string line;
	if (in.is_open()) {
		Mtl currentMtl("none");
		std::vector<Face> faces;
		do {
			getline(in, line);
			line = trim(line);
			if ((line.length() == 0)
					|| (line.find_first_not_of(" \t\r\n") == std::string::npos)
					|| (line[0] == '#')) {
				continue;
			}
			std::vector<std::string> parts = split(line, ' ');
			if (parts[0] == "newmtl") {
				currentMtl = Mtl(parts[1]);
			} else if (parts[0] == "Ns") {
				currentMtl.setSpecular((GLfloat) atof(parts[1].c_str()));
			} else if (parts[0] == "Ka") {
				std::vector<GLfloat> Ka;
				for(int i = 1; i <= 3; i++) Ka.push_back(atof(parts[i].c_str()));
				currentMtl.setSpecularColor(Ka);
			} else if (parts[0] == "Kd") {
				std::vector<GLfloat> Kd;
				for(int i = 1; i <= 3; i++) Kd.push_back(atof(parts[i].c_str()));
				currentMtl.setSpecularColor(Kd);
			} else if (parts[0] == "Ks") {
				std::vector<GLfloat> Ks;
				for(int i = 1; i <= 3; i++) Ks.push_back(atof(parts[i].c_str()));
				currentMtl.setSpecularColor(Ks);
			} else if (parts[0] == "Ni") {
				currentMtl.setIOR((GLfloat) atof(parts[1].c_str()));
			} else if (parts[0] == "d") {
				currentMtl.setTransparency((GLfloat) atof(parts[1].c_str()));
			} else if (parts[0] == "illum") {
				currentMtl.setIllumModel(atoi(parts[1].c_str()));
			}
		} while (in);
	}
	in.close();
}
