/*
 * Game.h
 *
 *  Created on: Jul 3, 2013
 *      Author: pat
 */

#ifndef GAME_H_
#define GAME_H_

#include "includes.h"
#include "WaveFront.h"

class Game {
public:
	static const int WIDTH = 1000;
	static const int HEIGHT = 700;
	static std::vector<GLbyte> keys;
	Game(GLFWwindow* window);
	virtual ~Game();
	void display();
	void start();
	static void handleMouseMove(GLFWwindow* window, double xpos, double ypos);
	static void handleKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void handleResize(GLFWwindow* window, int width, int height);
private:
	GLFWwindow* window;
	WaveFront model;
	static double mouseX;
	static double mouseY;
	double angle;
	double angle2;
};

#endif /* GAME_H_ */
