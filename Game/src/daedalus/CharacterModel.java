package daedalus;

import static org.lwjgl.opengl.GL11.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.util.glu.Sphere;

public class CharacterModel {
	private static int _bone_draw_list = -1;
	private double[][] _head_verts;
	private int _head_segments = 21;
	private double _head_radius = 0.25;
	private double[] _rotation_head;
	private double[] _rotation_arm_left_upper;
	private double[] _rotation_arm_left_lower;
	private double[] _rotation_arm_right_upper;
	private double[] _rotation_arm_right_lower;
	private double[] _rotation_back;
	private double[] _rotation_leg_left_upper;
	private double[] _rotation_leg_left_lower;
	private double[] _rotation_leg_right_upper;
	private double[] _rotation_leg_right_lower;
	private double _shoulder_radius = 0.25;
	private double _torso_radius = 0.1;
	private Obj torso;

	public CharacterModel() {
		if (_bone_draw_list == -1)
			_bone_draw_list = _createBoneDrawList();
		_head_verts = new double[_head_segments][3];
		for (int i = 0; i < _head_segments; i++) {
			_head_verts[i][0] = Math
					.cos(i * Math.PI * 2 / (_head_segments - 1)) * _head_radius;
			_head_verts[i][1] = Math.cos(i * Math.PI) * 0.15 + 0.5;
			_head_verts[i][2] = Math
					.sin(i * Math.PI * 2 / (_head_segments - 1)) * _head_radius;
		}
		_rotation_head = new double[4];
		_rotation_arm_left_upper = new double[4];
		_rotation_arm_left_lower = new double[4];
		_rotation_arm_right_upper = new double[4];
		_rotation_arm_right_lower = new double[4];
		_rotation_back = new double[4];
		_rotation_leg_left_upper = new double[4];
		_rotation_leg_left_lower = new double[4];
		_rotation_leg_right_upper = new double[4];
		_rotation_leg_right_lower = new double[4];
		torso = new Obj("/daedalus/torso.obj", true);
	}

	public void draw() {
		glTranslated(0, 1, 0);

		// ---BEGIN UPPER BODY---//
		glPushMatrix();
		{
			glColor3d(0.1, 0.3, 0.1);

			// Back
			torso.draw(_rotation_back, 1, true);

			// Head
			glPushMatrix();
			{
				glScaled(0.5, 0.5, 0.5);
				_drawHead(_rotation_head);
			}
			glPopMatrix();

			// Arms
			glPushMatrix();
			{
				glTranslated(0, -_shoulder_radius, 0);

				
				
				// Right arm
				glPushMatrix();
				{
					glTranslated(-_shoulder_radius, 0, 0);
					// Upper Arm
					glColor3d(0.1, 0.5, 0.1);
					_drawBone(_rotation_arm_right_upper, 0.6, false);
					// Lower Arm
					glColor3d(0.1, 0.1, 0.5);
					_drawBone(_rotation_arm_right_lower, 0.6, false);
				}
				glPopMatrix();

				// Left arm
				glPushMatrix();
				{
					glTranslated(_shoulder_radius, 0, 0);
					// Upper Arm
					glColor3d(0.1, 0.5, 0.1);
					_drawBone(_rotation_arm_left_upper, 0.6, false);
					// Lower arm
					glColor3d(0.1, 0.1, 0.5);
					_drawBone(_rotation_arm_left_lower, 0.6, false);
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
		// ---END UPPER BODY---//

		// ---BEGIN LOWER BODY---//
		{
			// Right leg
			glPushMatrix();
			{
				glTranslated(-_torso_radius, 0, 0);
				// Upper leg
				glColor3d(0.5, 0.5, 0);
				_drawBone(_rotation_leg_right_upper, 0.7, false);
				// Lower leg
				glColor3d(0, 0.5, 0.5);
				_drawBone(_rotation_leg_right_lower, 0.6, false);
			}
			glPopMatrix();

			// Left leg
			glPushMatrix();
			{
				glTranslated(_torso_radius, 0, 0);
				// Upper leg
				glColor3d(0.5, 0.5, 0);
				_drawBone(_rotation_leg_left_upper, 0.7, false);
				// Lower leg
				glColor3d(0, 0.5, 0.5);
				_drawBone(_rotation_leg_left_lower, 0.6, false);
			}
			glPopMatrix();
		}
		// ---END LOWER BODY---//
	}

	public void tick() {
	}

	private void _drawHead(double[] rotation) {
		if (rotation.length == 4)
			glRotated(rotation[3], 0, -1, 0);
		glRotated(rotation[0], -1, 0, 0);
		glRotated(rotation[1], 0, -1, 0);
		glRotated(rotation[2], 0, 0, 1);

		glBegin(GL_TRIANGLE_FAN);
		glColor4d(0, 0, 0, 1);
		glPointSize(10f);
		glVertex3d(0, 1, 0);
		glColor4d(0, 1, 0.5, 1.75);
		for (int i = _head_segments - 1; i >= 0; i--) {
			glVertex3d(_head_verts[i][0], _head_verts[i][1], _head_verts[i][2]);
		}
		glEnd();
		glBegin(GL_TRIANGLE_FAN);
		glPointSize(10f);
		glColor4d(0, 0, 0, 1);
		glVertex3d(0, 0, 0);
		glColor4d(0, 1, 0.5, 1.75);
		for (int i = 0; i < _head_segments; i++) {
			glVertex3d(_head_verts[i][0], _head_verts[i][1], _head_verts[i][2]);
		}
		glEnd();
	}

	private void _drawBone(double[] rotation, double scale, boolean up) {
		if (rotation.length == 4)
			glRotated(rotation[3], 0, -1, 0);
		glRotated(rotation[0], -1, 0, 0);
		glRotated(rotation[1], 0, -1, 0);
		glRotated(rotation[2], 0, 0, 1);

		if (!up)
			glTranslated(0, -scale, 0);
		glPushMatrix();
		glScaled(scale, scale, scale);
		glCallList(_bone_draw_list);
		glPopMatrix();
		if (up)
			glTranslated(0, scale, 0);
	}

	private static int _createBoneDrawList() {
		int boneList = glGenLists(1);

		glNewList(boneList, GL_COMPILE);
		
		glBegin(GL_TRIANGLE_FAN);
		// Lower half
		glVertex3d(0, 0, 0);
		glVertex3d(0.15, 0.75, 0);
		glVertex3d(0, 0.75, 0.1);
		glVertex3d(-0.15, 0.75, 0);
		glVertex3d(0, 0.75, -0.2);
		glVertex3d(0.15, 0.75, 0);
		glEnd();
		
		glBegin(GL_TRIANGLE_FAN);
		// Upper half
		glVertex3d(0, 1, 0);
		glVertex3d(0.15, 0.75, 0);
		glVertex3d(0, 0.75, -0.2);
		glVertex3d(-0.15, 0.75, 0);
		glVertex3d(0, 0.75, 0.1);
		glVertex3d(0.15, 0.75, 0);
		glEnd();
		
		glEndList();

		return boneList;
	}

	private static int _createBackDrawList() {
		int boneList = glGenLists(1);

		glNewList(boneList, GL_COMPILE);
		
		glBegin(GL_TRIANGLE_FAN);
		// Lower half
		glVertex3d(0, 0, 0);
		glVertex3d(0.15, 0.75, 0);
		glVertex3d(0, 0.75, 0.1);
		glVertex3d(-0.15, 0.75, 0);
		glVertex3d(0, 0.75, -0.2);
		glVertex3d(0.15, 0.75, 0);
		glEnd();
		
		glBegin(GL_TRIANGLE_FAN);
		// Upper half
		glVertex3d(0, 1, 0);
		glVertex3d(0.15, 0.75, 0);
		glVertex3d(0, 0.75, -0.2);
		glVertex3d(-0.15, 0.75, 0);
		glVertex3d(0, 0.75, 0.1);
		glVertex3d(0.15, 0.75, 0);
		glEnd();
		
		glEndList();

		return boneList;
	}

	/*
	 * With the exception of syntax, setting up vertex and fragment shaders is
	 * the same.
	 * 
	 * @param the name and path to the vertex shader
	 */
	private int createShader(String filename, int shaderType) throws Exception {
		int shader = 0;
		try {
			shader = ARBShaderObjects.glCreateShaderObjectARB(shaderType);

			if (shader == 0)
				return 0;

			ARBShaderObjects.glShaderSourceARB(shader,
					readFileAsString(filename));
			ARBShaderObjects.glCompileShaderARB(shader);

			if (ARBShaderObjects.glGetObjectParameteriARB(shader,
					ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL_FALSE)
				throw new RuntimeException("Error creating shader: "
						+ getLogInfo(shader));

			return shader;
		} catch (Exception exc) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			throw exc;
		}
	}

	private static String getLogInfo(int obj) {
		return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects
				.glGetObjectParameteriARB(obj,
						ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
	}

	private String readFileAsString(String filename) throws Exception {
		StringBuilder source = new StringBuilder();

		FileInputStream in = new FileInputStream(filename);

		Exception exception = null;

		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

			Exception innerExc = null;
			try {
				String line;
				while ((line = reader.readLine()) != null)
					source.append(line).append('\n');
			} catch (Exception exc) {
				exception = exc;
			} finally {
				try {
					reader.close();
				} catch (Exception exc) {
					if (innerExc == null)
						innerExc = exc;
					else
						exc.printStackTrace();
				}
			}

			if (innerExc != null)
				throw innerExc;
		} catch (Exception exc) {
			exception = exc;
		} finally {
			try {
				in.close();
			} catch (Exception exc) {
				if (exception == null)
					exception = exc;
				else
					exc.printStackTrace();
			}

			if (exception != null)
				throw exception;
		}

		return source.toString();
	}
}
