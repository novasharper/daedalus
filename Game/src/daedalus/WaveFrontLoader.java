package daedalus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class WaveFrontLoader {
	private static String frag_shader = "void main() { gl_FragColor = vec4(0, 1, 0, 1); }";
	
	private static class Face {
		private WaveFrontMtl mtl;
		private IntBuffer vIndices;
		
		public Face(WaveFrontMtl mtl, IntBuffer vIndices) {
			this.mtl = mtl;
			this.vIndices = vIndices;
		}
		
		public void draw() {
//			if(mtl != null) mtl.bind();
			GL11.glDrawElements(GL11.GL_TRIANGLE_FAN, vIndices);
		}
	}
	
	public static class WaveFrontObj {
		public final String name;
		private ArrayList<Face> faces;
		private boolean finished;
		
		public WaveFrontObj(String name){
			this.name = name;
			this.faces = new ArrayList<Face>();
			finished = false;
		}
		
		public void draw() {
			for(Face f : faces) f.draw();
		}
	}
	
	public static class WaveFrontMtl {
		private float ns; // Specularity constant
		private FloatBuffer ka; // Ambient color
		private FloatBuffer kd; // Diffuse color
		private FloatBuffer ks; // Specular color
		private float ni; // Index of refraction
		private float d; // Transparency
		private int illum; // Illumination model
		private WaveFront parent;
		
		public WaveFrontMtl(WaveFront parent, float ns, float[] ka, float[] kd, float[] ks, float ni, float d, int illum) {
			this.parent = parent;
			this.ns = ns;
			
			this.ka = BufferUtils.createFloatBuffer(4);
			this.ka.put(ka);
			this.ka.rewind();
			
			this.kd = BufferUtils.createFloatBuffer(4);
			this.kd.put(kd);
			this.kd.rewind();
			
			this.ks = BufferUtils.createFloatBuffer(4);
			this.ks.put(ks);
			this.ks.rewind();
			
			this.ni = ni;
			this.d = d;
			this.illum = illum;
		}
		
		public void bind() {
			GL20.glUniform1f(parent.getRef("ns"), ns);
			GL20.glUniform4(parent.getRef("ka"), ka);
			GL20.glUniform4(parent.getRef("kd"), kd);
			GL20.glUniform4(parent.getRef("ks"), ks);
			GL20.glUniform1f(parent.getRef("ni"), ni);
			GL20.glUniform1f(parent.getRef("d"), d);
			GL20.glUniform1i(parent.getRef("illum"), illum);
		}
	}
	
	public static class WaveFront {
		private Map<String, WaveFrontObj> objects;
		private Map<String, WaveFrontMtl> materials;
		private FloatBuffer vertices;
		private ArrayList<Float> vTemp;
		private boolean finished;
		private int program = 0;
		private int _ns, _ka, _kd, _ks, _ni, _d, _illum;
		
		public WaveFront() {
			objects = new HashMap<String, WaveFrontObj>();
			materials = new HashMap<String, WaveFrontMtl>();
			vTemp = new ArrayList<Float>();
			finished = false;
		}
		
		public void finish() {
			if(finished) return;
			vertices = BufferUtils.createFloatBuffer(vTemp.size());
			for(Float i : vTemp) vertices.put(i.floatValue());
			vertices.rewind();
			program = ARBShaderObjects.glCreateProgramObjectARB();
			int frag = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
			ARBShaderObjects.glShaderSourceARB(frag, frag_shader);
			ARBShaderObjects.glCompileShaderARB(frag);
			ARBShaderObjects.glAttachObjectARB(program, frag);
			ARBShaderObjects.glLinkProgramARB(program);
			ARBShaderObjects.glValidateProgramARB(program);
			finished = true;
		}
		
		public void draw() {
			ARBShaderObjects.glUseProgramObjectARB(program);
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			GL11.glVertexPointer(3, 0, vertices);
			for(String key : objects.keySet()) {
				objects.get(key).draw();
			}
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
			ARBShaderObjects.glUseProgramObjectARB(0);
		}
		
		public int getRef(String refVal) {
			return GL20.glGetUniformLocation(program, refVal);
		}
		
		public WaveFrontObj getObj(String name) {
			if(objects == null) return null;
			return objects.get(name);
		}
		
		public WaveFrontMtl getMtl(String name) {
			if(materials == null) return null;
			return materials.get(name);
		}
	}
	
	public static WaveFront loadObj(File toRead) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(toRead));
			String line;
			WaveFront loaded = new WaveFront();
			WaveFrontMtl currentMtl = null;
			WaveFrontObj currentObj = null;
			while((line = reader.readLine()) != null) {
				line = line.trim();
				String[] parts = line.split("\\s+");
				if(line.startsWith("#") || line.isEmpty()) continue;
				else if(parts[0].equals("mtllib")) {
					File mtlLibF = new File(toRead.getParentFile(), parts[1]);
					loadMtl(mtlLibF, loaded);
				}
				else if(parts[0].equals("usemtl")) {
					currentMtl = loaded.getMtl(parts[1]);
				}
				else if(parts[0].equals("o")) {
					if(currentObj != null) {
						loaded.objects.put(currentObj.name, currentObj);
					}
					currentObj = new WaveFrontObj(parts[1]);
				}
				else if(parts[0].equals("v")) {
					if(parts.length != 4) throw new Exception();
					for(int i = 1; i <= 3; i++) {
						loaded.vTemp.add(new Float(parts[i]));
					}
				}
				else if(parts[0].equals("f")) {
					int[] v = new int[parts.length - 1];
					for(int i = 1; i < parts.length; i++) {
						String[] aspects = parts[i].split("/");
						v[i - 1] = Integer.parseInt(aspects[0]) - 1;
					}
					IntBuffer vt = BufferUtils.createIntBuffer(v.length);
					vt.put(v);
					vt.rewind();
					currentObj.faces.add(new Face(currentMtl, vt));
				}
			}
			// Last object not added by default
			loaded.objects.put(currentObj.name, currentObj);
			loaded.finish();
			reader.close();
			
			return loaded;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Invalid format.");
		} finally {
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {}
			}
		}
		return null;
	}
	
	public static void loadMtl(File mtlFile, WaveFront loaded) {
		float ns;
		float[] ka;
		float[] kd;
		float[] ks;
		float ni;
		float d;
		int illum;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(mtlFile));
			String line;
			WaveFrontMtl currentMtl = null;
			while((line = reader.readLine()) != null) {
				line = line.trim();
				String[] parts = line.split("\\s+");
				if(line.startsWith("#") || line.isEmpty()) continue;
				else if(parts[0].equals("newmtl")) {
					
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Invalid format.");
		} finally {
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {}
			}
		}
	}
}
