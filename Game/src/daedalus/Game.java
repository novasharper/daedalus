package daedalus;

import java.awt.Canvas;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.JFrame;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;

import daedalus.WaveFrontLoader.WaveFront;

import static org.lwjgl.opengl.GL11.*;

public class Game extends JFrame implements Runnable {
	private boolean running;
	private Canvas myCanvas;
	private float w = 1000, h = 700;
	private CharacterModel model;
	private Obj model2;
	private WaveFront model3;
	
	public Game() {
		myCanvas = new Canvas();
		add(myCanvas);
		myCanvas.setSize((int) w, (int) h);
		pack();
		setResizable(false);
		setTitle("Foo");
		new Thread(this).start();
	}
	
	private void setup() {
		setVisible(true);
		addWindowListener(new WindowListener() {
			public void windowOpened(WindowEvent arg0) {
			}
			public void windowIconified(WindowEvent arg0) {
			}
			public void windowDeiconified(WindowEvent arg0) {
			}
			public void windowDeactivated(WindowEvent arg0) {
			}
			public void windowClosing(WindowEvent arg0) {
				running = false;
			}
			public void windowClosed(WindowEvent arg0) {				
			}
			public void windowActivated(WindowEvent arg0) {
			}
		});
		try {
			Display.setParent(myCanvas);
			Display.setDisplayMode(new DisplayMode((int) w, (int) h));
			Display.setVSyncEnabled(true);
			Display.create(new PixelFormat(0, 8, 0, 2));
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective(60, w / h, 0.1f, 500f);
		glMatrixMode(GL_MODELVIEW);
		glClearColor(0.5f, 0.5f, 0.5f, 1f);
		model = new CharacterModel();
		model2 = new Obj("/daedalus/torso.obj", true);
		model3 = WaveFrontLoader.loadObj(new File("bin/daedalus/torso.obj"));
	}
	
	public void run() {
		float r = 3.0f, t = 0, t2 = 0;
		setup();
		running = true;
		//Mouse.setGrabbed(true);
		
		glEnable(GL_BLEND);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		
		while(running && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {			
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			
//			glLoadIdentity();
			glPushMatrix();
			GLU.gluLookAt(
					r * (float) Math.sin(t), 0.5f, r * (float) Math.cos(t),
					0f, 0.5f, 0f,
					0f, 1f, 0f);
			
			glRotated(t2 * 180 / Math.PI, r * (float) Math.sin(t + Math.PI / 2), 0, r * (float) Math.cos(t + Math.PI / 2));
			
			
			int sides = 60;
			glBegin(GL_TRIANGLE_STRIP);
			for(int i = 0; i <= sides; i++) {
				glColor3d(0, 0, 0);
				glVertex3d(Math.cos(i * Math.PI * 2 / sides), -0.2, Math.sin(i * Math.PI * 2 / sides));
				glColor3d(1, 1, 1);
				glVertex3d(0.3 * Math.cos(i * Math.PI * 2 / sides), -0.2, 0.3 * Math.sin(i * Math.PI * 2 / sides));
			}
			glEnd();
			
			
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glPushMatrix();
			model3.draw();
			glPopMatrix();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			
			glPopMatrix();
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) t -= 1. / 60;
			else if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) t += 1. / 60;
			if(Keyboard.isKeyDown(Keyboard.KEY_UP)) t2 += 1. / 120;
			else if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) t2 -= 1. / 120;
			if(Keyboard.isKeyDown(Keyboard.KEY_ADD)) r += 0.01;
			else if(Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT)) r -= 0.01;
			Display.update();
			Display.sync(60);
		}
		Display.destroy();
		System.exit(0);
	}
	
	public static void main(String[] args) {
		new Game();
	}
}
