package daedalus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import org.lwjgl.opengl.*;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class Obj {
	int render = -1;
	private Map<String, Integer> renderMap;
	private Map<String, Integer> matMap;
	
	public Obj(String path) {
		this(path, false);
	}

	public Obj(String path, boolean readFromArchive) {
		try {
			if (readFromArchive)
				load(Obj.class.getResourceAsStream(path));
			else
				load(new FileInputStream(path));
		} catch (Exception ex) {
		}
	}

	public Obj(File toRead) {
		try {
			load(new FileInputStream(toRead));
		} catch (Exception ex) {
		}
	}

	public Obj(InputStream toRead) {
		load(toRead);
	}

	private void load(InputStream toRead) {
		renderMap = new HashMap<String, Integer>();
		BufferedReader r = new BufferedReader(new InputStreamReader(toRead));
		String line;
		ArrayList<double[]> vertexBuffer = new ArrayList<double[]>();
		ArrayList<int[]> faceBuffer = new ArrayList<int[]>();
		try {
			char mode = 'v';
			while ((line = r.readLine()) != null) {
				String parts[] = line.split(" ");
				if (parts[0].equalsIgnoreCase("f"))
					mode = 'f';
				else if (parts[0].equalsIgnoreCase("v") && mode == 'f')
					return;
				else if(!parts[0].equalsIgnoreCase("v"))
					continue;
				if (mode == 'v') {
					double x = Double.parseDouble(parts[1]);
					double y = Double.parseDouble(parts[2]);
					double z = Double.parseDouble(parts[3]);
					vertexBuffer.add(new double[] { x, y, z });
				} else if (mode == 'f') {
					int[] f = new int[parts.length - 1];
					for (int i = 0; i < f.length; i++)
						f[i] = Integer.parseInt(parts[i + 1]);
					faceBuffer.add(f);
				}
			}
		} catch (IOException e) {
			return;
		}
		render = glGenLists(1);
		glNewList(render, GL_COMPILE);

		for (int[] face : faceBuffer) {
			glBegin(GL_TRIANGLE_FAN);
			
			for (int vI : face) {
				double[] v = vertexBuffer.get(vI-1);
				glVertex3d(v[0], v[1], v[2]);
			}

			glEnd();
		}

		glEndList();
	}

	public void draw(double[] rotation, double scale, boolean up) {
		if(render == -1) return;
		if (rotation.length == 4)
			glRotated(rotation[3], 0, -1, 0);
		glRotated(rotation[0], -1, 0, 0);
		glRotated(rotation[1], 0, -1, 0);
		glRotated(rotation[2], 0, 0, 1);


		if (!up)
			glTranslated(0, -scale, 0);
		glPushMatrix();
		glScaled(scale, scale, scale);
		glCallList(render);
		glPopMatrix();
		if (up)
			glTranslated(0, scale, 0);
	}
}
